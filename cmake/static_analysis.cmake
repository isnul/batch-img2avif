if(CMAKE_GENERATOR MATCHES "Visual Studio")
  message(AUTHOR_WARNING "VS generator detected - skipping cppcheck, clang-tidy, cpplint")
else()
  find_program(CPPCHECK NAMES cppcheck)
  if(CPPCHECK)
    message(STATUS "Found cppcheck")
    set(CMAKE_CXX_CPPCHECK "${CPPCHECK}")
    list(APPEND CMAKE_CXX_CPPCHECK "--enable=all" "--inline-suppr" "--suppress=missingIncludeSystem")
  else()
    message(STATUS "Not found cppcheck")
  endif()

  find_program(CLANG_TIDY NAMES clang-tidy)
  if(CLANG_TIDY)
    message(STATUS "Found clang-tidy")
    set(CMAKE_CXX_CLANG_TIDY "${CLANG_TIDY}")
    list(APPEND CMAKE_CXX_CLANG_TIDY "--quiet" "--format-style=file" "--fix")
  else()
    message(STATUS "Not found clang-tidy")
  endif()

  find_program(CPPLINT NAMES cpplint)
  if(CPPLINT)
    message(STATUS "Found cpplint")
    set(CMAKE_CXX_CPPLINT "${CPPLINT}")
  else()
    message(STATUS "Not found cpplint")
  endif()
endif()