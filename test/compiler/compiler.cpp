#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest/doctest.h>

#include <cmath>

TEST_CASE("Testing compiler") {
    CHECK(static_cast<int>(std::pow(10, 2)) == 100);
}