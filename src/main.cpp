#include <iostream>
#include <fstream>
#include <filesystem>
#include <string>
#include <vector>
#include <array>
#include <memory>
#include <utility>
#include <cstdint>
#include <optional>
#include <stdexcept>
#include <algorithm>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <fmt/color.h>
#include <thread>
#include <chrono>

#include <jpeglib.h>
#include <lodepng.h>
#include <avif/avif.h>
#include <webp/decode.h>

namespace fs = std::filesystem;

const auto kOutputPathColor = fmt::terminal_color::yellow;
const auto kQualityColor = fmt::terminal_color::bright_cyan;
const auto k2400hColor = fmt::terminal_color::green;
const auto kNot2400hColor = fmt::terminal_color::red;
const auto kTimeSpanColor = fmt::terminal_color::bright_blue;
const auto kTimeSpanTotalColor = fmt::terminal_color::magenta;
const auto kRemainingTimeSpanColor = fmt::terminal_color::bright_magenta;
const auto kFileSizeColor = fmt::terminal_color::bright_blue;
const auto kTotalFileSizeColor = fmt::terminal_color::blue;
const auto kCompressRatioColor = fmt::terminal_color::bright_green;

constexpr int kRgbaChannels = 4;
constexpr int kRgbChannels = 3;
constexpr int kGrayscaleChannels = 1;
constexpr int kChannelBitDepth = 8;

constexpr int kDefaultAvifQuality = 40;

template <class T, class CustomerDeleter>
auto bakeSingleUnique(T* ptr, CustomerDeleter&& deleter) -> std::unique_ptr<T, CustomerDeleter> {
    return std::unique_ptr<T, CustomerDeleter>{ptr, std::forward<CustomerDeleter>(deleter)};
}

struct RawImageRgba {
    uint32_t width = 0;
    uint32_t height = 0;
    std::unique_ptr<uint8_t[]> data = nullptr; // NOLINT

    auto allocateData(int kNumChannels = kRgbaChannels) -> size_t {
        const size_t dataSize = static_cast<size_t>(width) * height * kNumChannels;
        data = std::make_unique<uint8_t[]>(dataSize); // NOLINT
        return dataSize;
    }
};
using DecodeImgResult = std::optional<RawImageRgba>;
using BinaryContent = std::vector<uint8_t>;

auto readBinaryFile(const fs::path& path) -> std::optional<BinaryContent> {
    if (std::ifstream ifs{path, std::ios::binary}) {
        ifs.unsetf(std::ios::skipws);
        ifs.seekg(0, std::ios::end);
        std::streampos file_size = ifs.tellg();
        ifs.seekg(0, std::ios::beg);
        BinaryContent res;
        res.reserve(file_size);
        res.insert(res.begin(), std::istream_iterator<uint8_t>{ifs}, std::istream_iterator<uint8_t>{});
        return res;
    }
    fmt::print(std::cerr, "readBinaryFile: Failed to open `{}`\n", path.generic_string());
    return {};
}

auto decodeJpeg(const BinaryContent& buffer) -> DecodeImgResult {
    jpeg_error_mgr jerr{};
    jpeg_decompress_struct cinfo{};
    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_decompress(&cinfo);
    jpeg_mem_src(&cinfo, buffer.data(), buffer.size());
    if (jpeg_read_header(&cinfo, TRUE) != JPEG_HEADER_OK) {
        fmt::print(std::cerr, "decodeJpeg: Failed to read jpeg header\n");
        return {};
    }
    jpeg_start_decompress(&cinfo);
    const int numChannels = cinfo.output_components;
    RawImageRgba temp;
    temp.width = cinfo.output_width;
    temp.height = cinfo.output_height;
    temp.allocateData(numChannels);
    const unsigned rowStride = temp.width * numChannels;
    while (cinfo.output_scanline < cinfo.output_height) {
        auto* bufferArray = temp.data.get() + cinfo.output_scanline * rowStride;
        jpeg_read_scanlines(&cinfo, &bufferArray, 1);
    }
    jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);
    RawImageRgba res{temp.width, temp.height};
    res.allocateData();
    if (numChannels == kGrayscaleChannels) {
        for (size_t i = 0; i < static_cast<size_t>(res.width) * res.height; i += kGrayscaleChannels) {
            res.data[kRgbaChannels * i + 0] = temp.data[i];
            res.data[kRgbaChannels * i + 1] = temp.data[i];
            res.data[kRgbaChannels * i + 2] = temp.data[i];
            res.data[kRgbaChannels * i + 3] = std::numeric_limits<uint8_t>::max();
        }
    } else if (numChannels == kRgbChannels) {
        for (size_t i = 0; i < static_cast<size_t>(res.width) * res.height; i += kRgbChannels) {
            const size_t j = i / kRgbChannels; // NOLINT
            res.data[kRgbaChannels * j + 0] = temp.data[i + 0];
            res.data[kRgbaChannels * j + 1] = temp.data[i + 1];
            res.data[kRgbaChannels * j + 2] = temp.data[i + 2];
            res.data[kRgbaChannels * i + 3] = std::numeric_limits<uint8_t>::max();
        }
    } else {
        fmt::print(std::cerr, "decodeJpeg: Number of channels is not grayscale or rgb\n");
        return {};
    }
    return res;
}

auto decodePng(const BinaryContent& buffer) -> DecodeImgResult {
    RawImageRgba res;
    uint8_t* outBuffer = nullptr;
    constexpr LodePNGColorType kColorType = LCT_RGBA;
    auto error = lodepng_decode_memory(&outBuffer, &res.width, &res.height, buffer.data(), buffer.size(), kColorType,
                                       kChannelBitDepth);
    if (outBuffer != nullptr && error == 0) {
        auto outBufferGuard = bakeSingleUnique(outBuffer, std::free); // cppcheck-suppress unreadVariable
        lodepng::State state;
        state.info_raw.colortype = kColorType;
        state.info_raw.bitdepth = kChannelBitDepth;
        const size_t outBufferSize = lodepng_get_raw_size(res.width, res.height, &state.info_raw);
        const size_t resBufferSize = res.allocateData();
        if (outBufferSize != resBufferSize) {
            fmt::print(std::cerr, "decodePng: LodePNG raw size {} != rgba buffer size {}\n", outBufferSize,
                       resBufferSize);
            return {};
        }
        std::copy(outBuffer, outBuffer + outBufferSize, res.data.get()); // NOLINT: in official example
        return res;
    }
    fmt::print(std::cerr, "decodePng: Failed to decode: {}\n", lodepng_error_text(error));
    return {};
}

auto decodeAvif(const BinaryContent& buffer) -> DecodeImgResult {
    RawImageRgba res;
    avifRGBImage rgb;
    std::memset(&rgb, 0, sizeof(rgb));
    auto decoder = bakeSingleUnique(avifDecoderCreate(), avifDecoderDestroy);
    if (auto errc = avifDecoderSetIOMemory(decoder.get(), buffer.data(), buffer.size()); errc != AVIF_RESULT_OK) {
        fmt::print(std::cerr, "decodeAvif: Fail to set io memory: {}\n", avifResultToString(errc));
        return {};
    }
    if (auto errc = avifDecoderParse(decoder.get()); errc != AVIF_RESULT_OK) {
        fmt::print(std::cerr, "decodeAvif: Failed to decode image: {}\n", avifResultToString(errc));
        return {};
    }
    while (avifDecoderNextImage(decoder.get()) == AVIF_RESULT_OK) {
        avifRGBImageSetDefaults(&rgb, decoder->image);
        res.width = rgb.width;
        res.height = rgb.height;
        res.allocateData();
        rgb.format = AVIF_RGB_FORMAT_RGBA;
        rgb.depth = kChannelBitDepth;
        rgb.rowBytes = rgb.width * kRgbaChannels;
        rgb.pixels = res.data.get();
        if (auto errc = avifImageYUVToRGB(decoder->image, &rgb); errc != AVIF_RESULT_OK) {
            fmt::print(std::cerr, "decodeAvif: Conversion from YUV failed: {}\n", avifResultToString(errc));
            return {};
        }
    }
    return res;
}

auto decodeWebp(const BinaryContent& buffer) -> DecodeImgResult {
    WebPBitstreamFeatures features;
    if (auto errc = WebPGetFeatures(buffer.data(), buffer.size(), &features); errc != VP8_STATUS_OK) {
        fmt::print(std::cerr, "decodeWebp: Failed to get features: {}\n", errc);
        return {};
    }
    RawImageRgba res{static_cast<uint32_t>(features.width), static_cast<uint32_t>(features.height), nullptr};
    const int outputStride = static_cast<int>(res.width) * kRgbaChannels;
    const size_t outputBufferSize = res.allocateData();
    auto* out = WebPDecodeRGBAInto(buffer.data(), buffer.size(), res.data.get(), outputBufferSize, outputStride);
    if (out == nullptr) {
        fmt::print(std::cerr, "decodeWebp: Failed to decode RGBA\n");
        return {};
    }
    return res;
}

auto isSupportedImageType(const fs::path& path) -> bool {
    const auto ext = path.extension().string();
    return std::ranges::any_of(std::array{".png", ".jpg", ".jpeg", ".webp", ".avif"},
                               [&](std::string_view spExt) { return ext == spExt; });
}

auto decodeImage(const fs::path& path) -> DecodeImgResult {
    auto buffer = readBinaryFile(path);
    if (!buffer) return {};
    auto tryDecode = [&](const auto& methods) {
        DecodeImgResult res;
        for (size_t i = 0; i < methods.size() - 1; ++i) {
            res = methods[i].first(*buffer); // NOLINT: i is inbound
            if (res) return res;
            fmt::print(std::cerr, "decodeImage: Retry again, treating as {}...\n",
                       methods[i + 1].second); // NOLINT: i is inbound
        }
        res = methods.back().first(*buffer);
        if (!res) fmt::print(std::cerr, "decodeImage: Fail to decode {}\n", path.generic_string());
        return res;
    };
    using ImageDecoder = std::pair<DecodeImgResult (*)(const BinaryContent&), std::string_view>;
    ImageDecoder decPng{decodePng, "PNG"};
    ImageDecoder decJpeg{decodeJpeg, "JPEG"};
    ImageDecoder decAvif{decodeAvif, "AVIF"};
    ImageDecoder decWebp{decodeWebp, "WEBP"};
    const auto ext = path.extension().string();
    if (ext == ".png") return tryDecode(std::array{decPng, decJpeg, decWebp, decAvif});
    if (ext == ".jpg" || ext == ".jpeg") return tryDecode(std::array{decJpeg, decPng, decWebp, decAvif});
    if (ext == ".webp") return tryDecode(std::array{decWebp, decPng, decJpeg, decAvif});
    if (ext == ".avif") return tryDecode(std::array{decAvif, decPng, decJpeg, decWebp});
    return {};
}

auto encodeAvif(const RawImageRgba& img, int quality, const fs::path& outPath) -> size_t {
    auto outImg = bakeSingleUnique(avifImageCreate(img.width, img.height, kChannelBitDepth, AVIF_PIXEL_FORMAT_YUV444),
                                   avifImageDestroy);
    avifRGBImage rgb;
    memset(&rgb, 0, sizeof(rgb));
    avifRGBImageSetDefaults(&rgb, outImg.get());
    rgb.pixels = img.data.get();
    rgb.rowBytes = img.width * kRgbaChannels;
    rgb.format = AVIF_RGB_FORMAT_RGBA;
    if (auto errc = avifImageRGBToYUV(outImg.get(), &rgb); errc != AVIF_RESULT_OK) {
        fmt::print(std::cerr, "encodeAvif: Failed to convert to YUV(A): {}\n", avifResultToString(errc));
        return 0;
    }
    auto encoder = bakeSingleUnique(avifEncoderCreate(), avifEncoderDestroy);
    encoder->speed = AVIF_SPEED_SLOWEST;
    encoder->maxThreads = 4; // std::thread::hardware_concurrency()
    encoder->quality = quality;
    encoder->qualityAlpha = AVIF_QUALITY_LOSSLESS;
    avifRWData data = AVIF_DATA_EMPTY;
    if (auto errc = avifEncoderWrite(encoder.get(), outImg.get(), &data); errc != AVIF_RESULT_OK) {
        fmt::print(std::cerr, "encodeAvif: Failed to encode: {}\n", avifResultToString(errc));
        return 0;
    }
    auto dataGuard = bakeSingleUnique(&data, avifRWDataFree); // cppcheck-suppress unreadVariable
    std::ofstream ofs{outPath, std::ios::binary};
    if (!ofs) {
        fmt::print(std::cerr, "encodeAvif: Failed to open `{}`\n", outPath.generic_string());
        return 0;
    }
    if (!ofs.write(static_cast<const char*>(static_cast<void*>(data.data)), static_cast<std::streamsize>(data.size))) {
        fmt::print(std::cerr, "encodeAvif: Failed to write `{}`\n", outPath.generic_string());
        return 0;
    }
    return data.size;
}

template <class DurationType>
struct StopWatch {
    using time_point_t = std::chrono::steady_clock::time_point;
    using time_span_t = DurationType;
    time_point_t firstStart;
    time_point_t start;
    StopWatch() : firstStart{std::chrono::steady_clock::now()}, start{firstStart} {}
    void restart() { start = std::chrono::steady_clock::now(); }
    auto read() const -> time_span_t {
        return std::chrono::duration_cast<time_span_t>(std::chrono::steady_clock::now() - start);
    }
    auto readTotal() const -> time_span_t {
        return std::chrono::duration_cast<time_span_t>(std::chrono::steady_clock::now() - firstStart);
    }
};

auto prettyPrintDuration(double totalSeconds) -> std::string {
    constexpr int kSecPerMin = 60;
    constexpr int kMinPerHour = 60;
    int minElapsed = static_cast<int>(totalSeconds / kSecPerMin);
    double secElapsed = totalSeconds - minElapsed * kSecPerMin;
    int hourElapsed = minElapsed / kMinPerHour;
    minElapsed %= kMinPerHour;
    std::string res;
    if (hourElapsed > 0) res += fmt::format("{}h", hourElapsed);
    if (minElapsed > 0) res += fmt::format("{}m", minElapsed);
    res += fmt::format("{:.3f}s", secElapsed);
    return res;
}

auto prettyPrintFileSize(size_t size) -> std::string {
    constexpr size_t KiB = 1024;
    constexpr size_t k10KiB = 10 * KiB;
    constexpr size_t k100KiB = 100 * KiB;
    constexpr size_t MiB = 1024 * KiB;
    constexpr size_t k10MiB = 10 * MiB;
    constexpr size_t k100MiB = 100 * MiB;
    constexpr size_t GiB = 1024 * MiB;
    if (size >= GiB) return fmt::format("{:.2f} GiB", static_cast<double>(size) / GiB);
    if (size >= k100MiB) return fmt::format("{} MiB", static_cast<size_t>(std::round(static_cast<double>(size) / MiB)));
    if (size >= k10MiB) return fmt::format("{:.1f} MiB", static_cast<double>(size) / MiB);
    if (size >= MiB) return fmt::format("{:.2f} MiB", static_cast<double>(size) / MiB);
    if (size >= k100KiB) return fmt::format("{} KB", static_cast<size_t>(std::round(static_cast<double>(size) / KiB)));
    if (size >= k10KiB) return fmt::format("{:.1f} KB", static_cast<double>(size) / KiB);
    if (size >= KiB) return fmt::format("{:.2f} KB", static_cast<double>(size) / KiB);
    return fmt::format("{} byte{}", size, size == 1 ? "" : "s");
}

struct Stats {
    StopWatch<std::chrono::duration<double>> watch;
    int numOfPages = 0;
    size_t totalFileSize = 0;
    size_t totalOriginalSize = 0;
    int successCnt = 0;
    int failureCnt = 0;
    int existedCnt = 0;
    int unrecognizedCnt = 0;
    size_t lastFileSize = 0;

    void collectUnrecognized() { ++unrecognizedCnt; }
    void collect(size_t pageFileSize, size_t inFileSize, bool existed) {
        (existed ? existedCnt : pageFileSize > 0 ? successCnt : failureCnt)++;
        lastFileSize = pageFileSize;
        totalFileSize += pageFileSize;
        totalOriginalSize += inFileSize;
    }
    auto estimateTimeLeft() const -> double {
        const int numOfPagesRemaining = numOfPages - successCnt - failureCnt - existedCnt;
        if (successCnt == 0) {
            if (existedCnt == 0) return 0;
            return watch.readTotal().count() / existedCnt * numOfPagesRemaining;
        }
        return watch.readTotal().count() / successCnt * numOfPagesRemaining;
    }
    void report() const {
        fmt::print("Done - output size {} (total {})\n",
                   fmt::styled(prettyPrintFileSize(lastFileSize), fmt::fg(kFileSizeColor)),
                   fmt::styled(prettyPrintFileSize(totalFileSize), fmt::fg(kTotalFileSizeColor)));
        fmt::print("       in {} (total {}), time left {}\n",
                   fmt::styled(prettyPrintDuration(watch.read().count()), fmt::fg(kTimeSpanColor)),
                   fmt::styled(prettyPrintDuration(watch.readTotal().count()), fmt::fg(kTimeSpanTotalColor)),
                   fmt::styled(prettyPrintDuration(estimateTimeLeft()), fmt::fg(kRemainingTimeSpanColor)));
        fmt::print("       compress ratio: {:.02f}% ({} / {})\n",
                   fmt::styled(100.0 * totalFileSize / totalOriginalSize, fmt::fg(kCompressRatioColor)), // NOLINT
                   fmt::styled(prettyPrintFileSize(totalFileSize), fmt::fg(kTotalFileSizeColor)),
                   fmt::styled(prettyPrintFileSize(totalOriginalSize), fmt::fg(kTotalFileSizeColor)));
    }
};

auto parseArgs(int argc, char** argv) -> std::optional<std::tuple<std::string, std::string, int>> {
    std::string inputDirPath;
    std::string outputDirPath;
    int quality = kDefaultAvifQuality;
    auto printUsage = [] { fmt::print("Usage: batch-png2avif [input folder] [output folder] [quality (0-100)]\n"); };
    if (argc != 4) {
        fmt::print("--- Manual input mode ---\nInput directory path: ");
        std::getline(std::cin, inputDirPath);
        fmt::print("Output directory path: ");
        std::getline(std::cin, outputDirPath);
        fmt::print("Quality 0-100 (default {}): ", quality);
        std::string input;
        std::getline(std::cin, input);
        if (!input.empty()) quality = std::stoi(input);
    } else {
        inputDirPath = argv[1];       // NOLINT
        outputDirPath = argv[2];      // NOLINT
        quality = std::stoi(argv[3]); // NOLINT
    }
    if (!fs::is_directory(inputDirPath)) {
        fmt::print("Error: Input directory is not a directory\n");
        printUsage();
        return {};
    }
    if (outputDirPath.empty() || fs::is_regular_file(outputDirPath)) {
        fmt::print("Error: Output directory is a file\n");
        printUsage();
        return {};
    }
    if (!fs::exists(outputDirPath)) fs::create_directories(outputDirPath);
    if (inputDirPath == outputDirPath) {
        fmt::print("Error: Output directory cannot be the same as input directory\n");
        printUsage();
        return {};
    }
    return std::make_tuple(inputDirPath, outputDirPath, quality);
}

auto main(int argc, char** argv) -> int {
    const auto args = parseArgs(argc, argv);
    if (!args) return -1;
    const auto [inputDirPath, outputDirPath, quality] = *args;
    const fs::path outPath{outputDirPath};
    Stats stats;
    for (const auto& dirEntry : std::filesystem::directory_iterator{inputDirPath}) {
        if (fs::is_directory(dirEntry)) continue;
        const auto& inFilePath = dirEntry.path();
        stats.numOfPages += static_cast<int>(isSupportedImageType(inFilePath));
    }
    fmt::print("Total pages found: {}\n", stats.numOfPages);
    for (const auto& dirEntry : std::filesystem::directory_iterator{inputDirPath}) {
        stats.watch.restart();
        if (fs::is_directory(dirEntry)) continue;
        const auto& inFilePath = dirEntry.path();
        if (!isSupportedImageType(inFilePath)) {
            stats.collectUnrecognized();
            fmt::print("Ignore unrecognized image type (png/jpeg/avif): `{}`\n", inFilePath.generic_string());
            continue;
        }
        const fs::path outputFilePath = outPath / (dirEntry.path().stem().generic_string() + ".avif");
        size_t outputFileSize = 0;
        size_t inputFileSize = fs::file_size(inFilePath);
        if (!fs::exists(outputFilePath)) {
            if (auto img = decodeImage(inFilePath)) {
                fmt::print("{}: {} to avif quality {}\n",
                           fmt::styled(outputFilePath.generic_string(), fmt::fg(kOutputPathColor)),
                           fmt::styled(fmt::format("{}x{}", img->width, img->height),
                                       fmt::fg(img->height == 2400 ? k2400hColor : kNot2400hColor)), // NOLINT
                           fmt::styled(quality, fmt::fg(kQualityColor)));
                stats.collect(outputFileSize = encodeAvif(*img, quality, outputFilePath), inputFileSize, false);
            } else {
                fmt::print(std::cerr, "Failed to decode `{}`\n", dirEntry.path().generic_string());
                return -1;
            }
        } else {
            stats.collect(outputFileSize = fs::file_size(outputFilePath), inputFileSize, true);
        }
        stats.report();
        if (outputFileSize == 0) {
            fmt::print(std::cerr, "Failed to encode `{}`\n", outputFilePath.generic_string());
            return -1;
        }
    }
}